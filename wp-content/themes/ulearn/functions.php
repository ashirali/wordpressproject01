<?php

function ulearn_script_enqueue() {
    wp_enqueue_style('customstyle', get_template_directory_uri().'/css/style.css', array(), '1', 'all');
    //wp_enqueue_style('customjs', get_template_directory_uri().'/js/ulearn.js', array(), '1', 'all'); for js
}
add_action('wp_enqueue_scripts', 'ulearn_script_enqueue');


function add_footer_styles_font_awesome() {
    wp_enqueue_style('fontawesome5', get_template_directory_uri().'/libraries/fontawesome/css/all.min.css', array(), null );
};
add_action( 'wp_enqueue_scripts', 'add_footer_styles_font_awesome' );


//Handle data retrieved from a social network profile
function oa_social_login_store_extended_data ($user_data, $identity)
{
  // $user_data is an object that represents the newly added user
  // The format is similar to the data returned by $user_data = get_userdata ($user_id);
 
  // $identity is an object that contains the full social network profile
   
  //Example to store the gender
  update_user_meta ($user_data->ID, 'gender', $identity->gender);
}
 
//This action is called whenever Social Login adds a new user
add_action ('oa_social_login_action_after_user_insert', 'oa_social_login_store_extended_data', 10, 2);

?>

    
          
        
    

    
        
        
    
    
    
        
        
    
    

    
            
        
 