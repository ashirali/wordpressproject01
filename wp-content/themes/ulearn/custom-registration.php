<?php
/* Template Name: Custom Registration Page */
get_header(); 

global $wpregister;

if($_POST){
    $firstname = $wpregister ->escape($_POST['firstName']);
    $lastname = $wpregister ->escape($_POST['lastName']);
    $email = $wpregister ->escape($_POST['userEmail']);
    $password = $wpregister ->escape($_POST['password']);
    $password = $wpregister ->escape($_POST['confirmPassword']);
if(strpos($firstname, ' ') !==FALSE) {
    $errormessage['firstName_space'] = ['No space is allowed in this field']
}
if(empty($firstname)) {
    $errormessage['firstName_empty'] = "It's a required field!";
}
if(strpos($lastname, ' ') !==FALSE) {
    $errormessage['lastName_space'] = ['No space is allowed in this field']
}
if(empty($lastname)) {
    $errormessage['lastName_empty'] = "It's a required field!";
}
}
?>
<div class="container jumbotron">
<form action="post">
  <div class="form-group">
    <label for="firstName">First Name</label>
    <input type="text" class="form-control" name="firstName" id="firstName" placeholder="Enter First Name">
  </div>
  <div class="form-group">
    <label for="lastName">Last Name</label>
    <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Enter Last Name">
  </div>
  <div class="form-group">
    <label for="userEmail">User Email</label>
    <input type="email" class="form-control" name="userEmail" id="userEmail" placeholder="User Email *">
  </div>
  <div class="form-group">
    <label for="password">User Password</label>
    <input type="password" class="form-control" name="password" id="password" placeholder="User Password *">
  </div>
  <div class="form-group">
    <label for="confirmPassword">First Name</label>
    <input type="password" class="form-control" name="confirmPassword" id="confirmPassword" placeholder="User Confrim Password *">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
<?php get_footer(); ?>